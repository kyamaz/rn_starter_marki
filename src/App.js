import {Navigation} from 'react-native-navigation'
//screens
import {AnimScreen} from './screens/Anim/Anim';
import {HomeScreen} from './screens/Home/Home';
import {StartTabs} from './screens/MainTabs/MainTabs';
import {MapScreen} from './screens/Map/Map';
import {CameraScreen} from './screens/Camera/Camera'
import {SideDrawerSrcreen} from './screens/Side-Drawer/Side-Drawer'


//icons 
import Icon from "react-native-vector-icons/Ionicons";

//screens registrations
Navigation.registerComponent('markI.StartTabs', ()=>StartTabs)
Navigation.registerComponent("markI.HomeScreen", ()=>HomeScreen)
Navigation.registerComponent("markI.AnimScreen", ()=>AnimScreen)
Navigation.registerComponent("markI.MapScreen", ()=>MapScreen)
Navigation.registerComponent("markI.CameraScreen", ()=>CameraScreen)
Navigation.registerComponent("markI.SideDrawerSrcreen", ()=>SideDrawerSrcreen)


Navigation.startSingleScreenApp({
  screen:{
    screen:"markI.HomeScreen",
    title:"home"
  }
})
