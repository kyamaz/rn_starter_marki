import React , {Component}from "react";
import { View, Animated , StyleSheet, Text } from "react-native";
class ball extends Component{

    constructor(prop){
        super(prop)
    }
    componentWillMount(){
        this.position=new Animated.ValueXY({x:-150,y:0})
        Animated.spring( this.position,{
            toValue:{x:300, y:0}
        }).start()
    }
    render(){ 
        return(
            <Animated.View style={[styles.container, this.position.getLayout()]}>
                <View style={styles.ball}  >
                </View>
            </Animated.View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        width:"100%",
        justifyContent:"flex-start"

    },
    ball:{
        height:60,
        width:60,
        borderRadius:30,
        borderWidth:30,
        borderColor:"orange"
    }
});
export { ball as Ball };