import React , {Component}from "react";
import { View, Animated , StyleSheet, PanResponder, Dimensions, LayoutAnimation, UIManager } from "react-native";
const SCREEN_WIDTH=Dimensions.get('window').width;
const SWIPE_THRESHOLD= SCREEN_WIDTH *0.33;
const SWIPE_OUT_DURATION= 500;

class deck extends Component{
    static defaultProps={
        onSwipeLeft:()=>{},
        onSwipeRight:()=>{}
    }
    constructor(props){
        super(props)
        let _position=new Animated.ValueXY()
        const _panResponder=PanResponder.create({
            onStartShouldSetPanResponder:()=>true,
            onPanResponderMove:(event, gesture)=>{
                _position.setValue({x: gesture.dx, y:gesture.dy })
            },
            onPanResponderRelease:(event, gesture)=>{
                if(gesture.dx>SWIPE_THRESHOLD){
                    this.forceSwipe('right');
                }else if(gesture.dx< -SWIPE_THRESHOLD){
                    this.forceSwipe('left');
                }else{
                    this.resetPosition();
                }
            }
        })
        this.state={_panResponder, _position, index:0} 
    }
    forceSwipe=(direction)=>{
        const x=direction==='right'?SCREEN_WIDTH*1.5:-SCREEN_WIDTH*1.5;
        Animated.timing(this.state._position,{
            toValue:{x, y:0},
            duration:SWIPE_OUT_DURATION
        }).start(()=>{ this.onSwipeComplete(direction) })
    }
    onSwipeComplete=(direction)=>{
        const {onSwipeLeft, onSwipeRight, data }=this.props
        const item = data[this.state.index]
        direction==='right'?onSwipeRight(item):onSwipeLeft(item);
        //animation does not go by state cycle 
        this.state._position.setValue({x:0, y:0})
        this.setState({
            index:this.state.index+1
        })


    }

    resetPosition=()=>{
        Animated.spring( this.state._position,{
            toValue:{x:0, y:0}
        }).start();
    }

    getCardStyle=()=>{
        const {_position}=this.state;
        const rotate=_position.x.interpolate({
            inputRange:[-SCREEN_WIDTH*1.5, 0, SCREEN_WIDTH*1.5],
            outputRange:['-60deg', '0deg', '60deg']
        })
        return {
            ..._position.getLayout(),
            transform:[{rotate} ]
        }
    }
    componentWillReceiveProps(nextProps){
        if(newProps.data !== this.props.data){
            this.setState({index:0})
        }

    }
    componentWillUpdate(){
        UIManager.setLayoutAnimationEnabledExperimental&& UIManager.setLayoutAnimationEnabledExperimental(true)
        LayoutAnimation.spring();
    }
    renderCards=()=>{

        if(this.state.index >= this.props.data.length){
          return  this.props.renderNoMoreCards();
        }
        return this.props.data.map( (datum, idx) => {
    
            if( idx < this.state.index){return null }
            if( this.state.index ===idx ){
                return(
                    <Animated.View 
                        key={datum.id}
                        style={[ this.getCardStyle(), styles.cardStyle ]}
                        {...this.state._panResponder.panHandlers}>
                        { this.props.renderCard(datum)}
                    </Animated.View> 
                )
            }
            return (
                <Animated.View key={datum.id} style={[styles.cardStyle, {zIndex:-1, top:10*(idx- this.state.index) }]}>
                    { this.props.renderCard( datum)}
                </Animated.View>
            )
        }).reverse()
    }
    render(){ 
        return(
            <View>
               { this.renderCards()}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    cardStyle:{
    position:'absolute',
    width:SCREEN_WIDTH
    }
});
export { deck as Deck };