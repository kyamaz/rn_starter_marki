import React, {Component} from "react";
import {  StyleSheet, View, Text, Animated, Image, PanResponder } from "react-native";
import {Deck} from './../../components/Deck/Deck';
import { Card, ListItem, Button,  } from 'react-native-elements'
import placeImage from './../../assets/imgs/download.jpeg'

const DATA = [
  { id: 1, text: 'Card #1', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 2, text: 'Card #2', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 3, text: 'Card #3', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 4, text: 'Card #4', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
  { id: 5, text: 'Card #5', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 6, text: 'Card #6', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 7, text: 'Card #7', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 8, text: 'Card #8', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
];


class anim  extends Component{
    constructor(props){
        super(props)
        this.props.navigator.setOnNavigatorEvent(this.OnNavigatorEvent)
    
      }
      OnNavigatorEvent=(event)=>{
        if(event.type === "NavBarButtonPress" ){
          if(event.id==="sideMenuToggle"){
            this.props.navigator.toggleDrawer({
              side:"left"
            })
          }
        }
    
      }

    renderCard=(item)=>{
      console.log( {uri:item.uri} )

      return(
             <Card title={ item.text}
             key={item.id}
             onSwipeLeft={()=>console.log('left')}
             onSwipeRight={()=>console.log('right')}
             image={{ uri: item.uri  }}>
              <Button
                backgroundColor='#03A9F4'
                buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
                title='VIEW NOW' />

            </Card>
      )
    }

    renderNoMoreCards(){
      console.log( 'no de')
      return (
        <Card title="Tinder is dead"
          image={{ uri: 'https://tinder.com/?lang=fr' }}>
        <Text>
          No more tindering
        </Text>
       </Card>
      )
    }
    render(){
        return(
            <View style={styles.container}  >
              <Deck data={DATA}
                renderNoMoreCards={this.renderNoMoreCards}
                renderCard={this.renderCard}   
             />
            </View>
        
        );
    }
}

const styles = StyleSheet.create({
  container:{
        backgroundColor: "#eee",
        flex:1
    },
    imagePreview:{
      width:"100%",
      height:"30%",
      minHeight:150
  },
});


export { anim as AnimScreen };