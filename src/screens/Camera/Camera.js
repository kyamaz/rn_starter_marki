
import React, {Component} from "react";
import { View, Text , StyleSheet, Button , Image, TouchableOpacity, Animated } from "react-native";
import ImagePicker from "./../../components/fixImage/fixImage"
class camera extends React.Component {
  constructor(props){
    super(props)
    this.props.navigator.setOnNavigatorEvent(this.OnNavigatorEvent)
  }
  componentWillMount(){
    this.position=new Animated.ValueXY({x:-350,y:0})
    Animated.spring( this.position,{
        toValue:{x:0, y:0}
    }).start()
}

  OnNavigatorEvent=(event)=>{
    if(event.type === "NavBarButtonPress" ){
      if(event.id==="sideMenuToggle"){
        this.props.navigator.toggleDrawer({
          side:"left"
        })
      }
    }
    //navigato life cycle
    switch(event.id) {
      case 'willAppear':
       break;
      case 'didAppear':
        break;
      case 'willDisappear':
        break;
      case 'didDisappear':
        break;
      case 'willCommitPreview':
        break;
    }
  }
  
  state={
    pickedImage:null
  }
  handleImage=()=>{
    ImagePicker.showImagePicker({res:"pibck image"},(res)=>{
      if(res.didCancel){
        return console.log('cancel')
      }
      if(res.error){
        return console.log('error')
      }
      console.log( res)
      this.setState({
        pickedImage:{uri:res.uri}
      })
    })

  }
    render() {
      console.log( this.state.pickedImage)
      return (
        <Animated.View style={this.position.getLayout()}>

          <Image source={this.state.pickedImage}  
            style={styles.imagePreview} />
            <Button title={ this.state.pickedImage?"change image":"pick image"} 
            onPress={this.handleImage} />
        </Animated.View>

      )
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    imagePreview:{
      width:"100%",
      height:"90%",
      minHeight:150
  },
  });
  
  export { camera as CameraScreen };