import {Navigation} from 'react-native-navigation';
import {MapScreen} from './../Map/Map';
import {AnimScreen} from './../Anim/Anim';
import {CameraScreen} from './../Camera/Camera'

import Icon from "react-native-vector-icons/Ionicons";

const startTabs=()=>{
    Promise.all([
        Icon.getImageSource('md-ionic', 30),
        Icon.getImageSource('md-egg', 30),
        Icon.getImageSource('md-basketball', 30),
        Icon.getImageSource('md-menu', 30),


    ]).then(
      icons=>{
        Navigation.startTabBasedApp({
          tabs:[{
              screen:"markI.MapScreen",
              title:"home",
              label:"home",
              icon:icons[0],
              navigatorButtons:{
                leftButtons:[
                  {
                    icon:icons[3],
                    title:"menu",
                    id:"sideMenuToggle"
                  }
                ]
              }
            },
            {
              screen:"markI.AnimScreen",
              title:"anim",
              label:"anim",
              icon:icons[1],
              leftButtons:[
                {
                  icon:icons[3],
                  title:"menu",
                  id:"sideMenuToggle"
                }
              ]
            },
            {
              screen:"markI.CameraScreen",
              title:"camera",
              label:"camera",
              icon:icons[2],
              navigatorButtons:{
                leftButtons:[
                  {
                    icon:icons[3],
                    title:"menu",
                    id:"sideMenuToggle"
                  }
                ]
              }
            }
          ],
          // to style tab, tabsStyle && appStyle(android)
          tabsStyle:{
            tabBarSelectedButtonColor:"orange"
          },
          appStyle:{
            tabBarSelectedButtonColor:"orange"
          },
          drawer:{
            left:{
              screen:"markI.SideDrawerSrcreen"
            },
            disableOpenGesture:true
          }
        })
      }
    )  
  }

export {startTabs as StartTabs}

  
